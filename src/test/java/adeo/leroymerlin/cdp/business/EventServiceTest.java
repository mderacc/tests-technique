package adeo.leroymerlin.cdp.business;

import adeo.leroymerlin.cdp.model.Band;
import adeo.leroymerlin.cdp.model.Event;
import adeo.leroymerlin.cdp.model.Member;
import adeo.leroymerlin.cdp.repository.EventRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class EventServiceTest {
    EventRepository mockedEventRepository;
    EventService eventService;
    List<Event> mockedEvents;

    @BeforeEach
    void setup() {
        mockedEventRepository = mock(EventRepository.class);
        eventService = new EventService(mockedEventRepository);
        mockedEvents = new ArrayList<>();

        Member member1 = new Member().setName("Member1");
        Member member2 = new Member().setName("Member2");
        Member member3 = new Member().setName("Member3");
        Member member4 = new Member().setName("Member4");
        Set<Member> membersBand1 = new HashSet<>();
        membersBand1.add(member1);
        membersBand1.add(member2);
        membersBand1.add(member3);
        Set<Member> membersBand2 = new HashSet<>();
        membersBand2.add(member4);
        Set<Member> membersBand3 = new HashSet<>();
        Band band1 = new Band().setName("Band1").setMembers(membersBand1);
        Set<Band> bandsEvent1 = new HashSet<>();
        bandsEvent1.add(band1);
        Band band2 = new Band().setName("Band2").setMembers(membersBand2);
        Set<Band> bandsEvent2 = new HashSet<>();
        bandsEvent2.add(band2);
        bandsEvent2.add(band1);
        Band band3 = new Band().setName("Band3").setMembers(membersBand3);
        Set<Band> bandsEvent3 = new HashSet<>();
        bandsEvent3.add(band3);

        Event event1 = new Event().setId(1l).setTitle("Event1").setBands(bandsEvent1);
        Event event2 = new Event().setId(2l).setTitle("Event2").setBands(bandsEvent2);
        Event event3 = new Event().setId(3l).setTitle("Event3").setBands(bandsEvent3);
        Event event4 = new Event().setId(4l).setTitle("Event4");

        mockedEvents.add(event1);
        mockedEvents.add(event2);
        mockedEvents.add(event4);
        mockedEvents.add(event3);
    }

    @Test
    @DisplayName("Given existing member name should return events with groups that contains given member")
    void given_existing_member_name_should_return_events_with_groups_that_contains_given_member() {
        // ARRANGE
        when(mockedEventRepository.findAll()).thenReturn(mockedEvents);

        // ACT
        final List<Event> events = eventService.getFilteredEvents("er1");

        // ASSERT
        assertThat(events.size()).isEqualTo(2);
        assertThat(events.get(0).getTitle()).isEqualTo("Event1 [1]");
        assertThat(events.get(0).getBands().size()).isEqualTo(1);
    }

    @Test
    @DisplayName("Given not existing member name should return empty list")
    void given_not_existing_member_name_should_return_empty_list() {
        // ARRANGE
        when(mockedEventRepository.findAll()).thenReturn(mockedEvents);

        // ACT
        final List<Event> events = eventService.getFilteredEvents("er8");

        // ASSERT
        assertThat(events.size()).isEqualTo(0);
    }

    @Test
    @DisplayName("Given events when get should return all events")
    void given_events_when_get_should_return_all_events() {
        // ARRANGE
        when(mockedEventRepository.findAll()).thenReturn(mockedEvents);

        // ACT
        final List<Event> events = eventService.getEvents();

        // ASSERT
        assertThat(events.size()).isEqualTo(4);
        assertThat(events.get(0).getTitle()).isEqualTo("Event1");
        assertThat(events.get(0).getBands().size()).isEqualTo(1);
        assertThat(events.get(1).getTitle()).isEqualTo("Event2");
        assertThat(events.get(1).getBands().size()).isEqualTo(2);
        assertThat(events.get(2).getTitle()).isEqualTo("Event4");
        assertThat(events.get(2).getBands()).isNull();
        assertThat(events.get(3).getTitle()).isEqualTo("Event3");
        assertThat(events.get(3).getBands().size()).isEqualTo(1);
    }

    @Test
    @DisplayName("Given not existing event id when delete should not invoke delete method")
    void given_not_existing_event_id_when_delete_should_not_invoke_delete_method() {
        eventService.delete(1000L);
        verify(mockedEventRepository, times(1)).exists(1000L);
        verify(mockedEventRepository, times(0)).delete(1000L);
    }
}
