Feature: CRUD Actions on Events

  Background:
    * def eventJson = read('data/updated-event.json')
    * def resourceUrl = apiBaseUrl + '/events'

  @bugfix
  Scenario: Update and remove event
    # Update event
    Given url resourceUrl + '/1000'
    And header Content-Type = 'application/json'
    And request eventJson
    When method PUT
    Then status 200

    # Search all events
    Given url resourceUrl + '/'
    When method GET
    Then status 200
    And match response == '#[5]'
    And match response contains deep {title: 'GrasPop Metal Meeting', nbStars: 3, comment: "test"}

    # Remove an event
    * call read('remove-step.feature') { id: '1000' }

    # Search all events
    Given url resourceUrl + '/'
    When method GET
    Then status 200
    And match response == '#[4]'

  @feature
  Scenario: Search unexisting brand member name should return empty list
    # Search by brand member name
    Given url resourceUrl + '/search/ZZZ'
    When method GET
    Then status 200
    And match response == '#[0]'

  @bugfix
  Scenario: Get all events after remove all should return empty list

    * call read('remove-step.feature') { id: '1001' }
    * call read('remove-step.feature') { id: '1002' }
    * call read('remove-step.feature') { id: '1003' }
    * call read('remove-step.feature') { id: '1004' }

    Given url resourceUrl + '/'
    When method GET
    Then status 200
    And match response == '#[0]'

