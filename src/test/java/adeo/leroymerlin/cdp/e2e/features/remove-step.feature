Feature: Remove event

  Background:
    * def deleteUrl = apiBaseUrl + '/events/' + id

  Scenario:
    Given url deleteUrl
    And header Content-Type = 'application/json'
    When method DELETE
    Then status 200
