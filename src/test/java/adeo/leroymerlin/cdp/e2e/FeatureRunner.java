package adeo.leroymerlin.cdp.e2e;

import com.intuit.karate.junit5.Karate;

public class FeatureRunner {
    @Karate.Test
    Karate testBugfixScenario() {
        return Karate.run("features/crud-scenario")
                .tags("@bugfix")
                .relativeTo(getClass());
    }

    @Karate.Test
    Karate testFeatureScenario() {
        return Karate.run("features/crud-scenario")
                .tags("@feature")
                .relativeTo(getClass());
    }
}
