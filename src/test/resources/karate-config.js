function init() {
    var config = {
        apiBaseUrl: 'http://localhost:8080/api'
    };
    karate.configure('connectTimeout', 5000);
    karate.configure('readTimeout', 5000);
    return config;
}
