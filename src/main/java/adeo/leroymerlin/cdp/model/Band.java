package adeo.leroymerlin.cdp.model;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Band {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @OneToMany(fetch=FetchType.EAGER, cascade=CascadeType.MERGE)
    private Set<Member> members;

    public Set<Member> getMembers() {
        return members;
    }

    public Band setMembers(Set<Member> members) {
        this.members = members;
        return this;
    }

    public String getName() {
        return name;
    }

    public Band setName(String name) {
        this.name = name;
        return this;
    }
}
