package adeo.leroymerlin.cdp.business;

import adeo.leroymerlin.cdp.model.Band;
import adeo.leroymerlin.cdp.model.Event;
import adeo.leroymerlin.cdp.model.Member;
import adeo.leroymerlin.cdp.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class EventService {

    private final EventRepository eventRepository;

    @Autowired
    public EventService(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public List<Event> getEvents() {
        List<Event> events = new ArrayList();
        eventRepository.findAll().forEach(event -> events.add(event));
        return events;
    }

    public void delete(Long id) {
        if(eventRepository.exists(id)) {
            eventRepository.delete(id);
        }
    }

    public List<Event> getFilteredEvents(String query) {
        return StreamSupport.stream(eventRepository.findAll().spliterator(), false)
            .map(event -> event.setBands(getFilteredBands(event, query)))
                .filter(event -> event.getBands().size() > 0)
                .map(event -> event.setTitle(event.getTitle() + " [" + event.getBands().size() + "]"))
                .collect(Collectors.toList());
    }

    private Set<Band> getFilteredBands(Event event, String query){
        return Optional.ofNullable(event.getBands())
            .map(bands -> bands.stream()
                .map(band -> band.setMembers(getFilteredMembers(band, query)))
                .filter(band -> band.getMembers().size() > 0)
                .map(band -> band.setName(band.getName() + " [" + band.getMembers().size() + "]"))
                .collect(Collectors.toSet()))
            .orElse(Collections.emptySet());
    }

    private Set<Member> getFilteredMembers(Band band, String query) {
        return Optional.ofNullable(band.getMembers())
            .map(members -> members.stream()
            .filter(member -> member.getName().contains(query))
            .collect(Collectors.toSet()))
        .orElse(Collections.emptySet());
    }

    public Event save(Event event) {
         return eventRepository.save(event);
    }
}
